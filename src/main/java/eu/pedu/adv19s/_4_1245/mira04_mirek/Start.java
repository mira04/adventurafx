/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.pedu.adv19s._4_1245.mira04_mirek;


import eu.pedu.adv19s._4_1245.mira04_mirek.game.MirSpaceGame;
import eu.pedu.adv19s._4_1245.mira04_mirek.game.MyController;
import eu.pedu.adv19s._4_1245.mira04_mirek.textui.UIA_JOptionPane;
import eu.pedu.adv19s_fw.game_txt.IGame;
import eu.pedu.adv19s_fw.game_txt.IUI;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Hlavní třída aplikace, potomkem třídy Application.
 * Obsahuje metodu main a stará se o spuštění hry.
 *
 * @author Albert Mírek
 */
public class Start extends Application
{



    /***************************************************************************
     * Hlavní metoda aplikace spouštějící textové či grafické rozhraní na základě vstupních parametrů
     *
     * @param args Parametry příkazového řádku
     */
    public static void main(String[] args)
    {
         if(args.length > 0 && args[0].equals("text")) {
            IUI ui = (IUI) new UIA_JOptionPane();
            ui.startGame();
        } else {
            launch(args);
        }
        launch(args);
    }


    /**
     * Inicializace primaryStage a FXML loaderu
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/scena.fxml"));
        Parent rootComponent = loader.load();

        Scene scene = new Scene(rootComponent);
        primaryStage.setScene(scene);

        IGame hra = new MirSpaceGame();
        hra.executeCommand("");

        MyController controller = loader.getController();
        controller.setHra(hra);


        primaryStage.setHeight(720);
        primaryStage.setWidth(1280);
        primaryStage.setTitle("Spaceship Expo");
        //ikona
        /*InputStream iconStream = getClass().getResourceAsStream();
        Image image = new Image(iconStream);
        primaryStage.getIcons().add(image);*/


        primaryStage.show();
    }
}
