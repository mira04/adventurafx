package eu.pedu.adv19s._4_1245.mira04_mirek.game;

import static eu.pedu.adv19s._4_1245.mira04_mirek.game.Texts.*;
import eu.pedu.adv19s_fw.game_txt.IGame;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


/**
 * Třída MyController se stará o korektní provedení logiky hry v grafickém rozhraní.
 */
public class MyController {

    /**
     * Statická konstanta udávající šířku obrázku předmětů a východů
     */
    public static final int SIRKA_IKONY = 45;

    /**
     * Statická konstanta udávající výšku obrázku předmětů a východů
     */
    public static final int VYSKA_IKONY = 30;

    public Label nazevLokace;
    public ImageView obrazekLokace;
    public VBox seznamVychodu;
    public VBox seznamLehkychPredmetuVMistnosti;
    public VBox seznamTezkychPredmetuVMistnosti;
    public VBox seznamPredmetuVBatohu;
    public TextArea odpovediProgramu;
    public GridPane sestaveniUkonu;
    public ComboBox vyberUkonu;
    public ComboBox vyberCil;
    public ComboBox vyberPredmet;
    public Button btnZpracuj;
    public Label lblCil;
    public Label lblPredmet;

    /**
     * instance třídy IGame
     */
    private IGame hra;

    /**
     * Inicializace hry
     * @param hra
     */
    public void setHra(IGame hra){
        this.hra = hra;

        SpaceShip herniplan = (SpaceShip) hra.getWorld();
        Room aktualniProstor = herniplan.getCurrentPlace();

        vyberUkonu.getItems().addAll(pNACTI,pROZSEKEJ,pZAPNI,pZASAĎ);

        nazevLokace.setText(aktualniProstor.getName());
        vypisOdpoved(zUVÍTÁNÍ);
        zmenProstor(aktualniProstor);

    }

    /**
     * Metoda zajišťující změnu prostoru
     * @param prostor
     */
    private void zmenProstor(Room prostor)
    {
        nazevLokace.setText(prostor.getName());
        vyberCil.setVisible(false);

        String nazevObrazku = "/"+prostor.getName().toLowerCase()+".jpg";
        Image image = new Image(getClass().getResourceAsStream(nazevObrazku));
        obrazekLokace.setImage(image);
        seznamVychodu.getChildren().clear();

        pridejPredmety(prostor);
        pridejVychody(prostor);
        nastavComboBox(prostor);
    }

    /**
     * Metoda získavající předměty aktuálního prostoru
     * @param prostor
     */
    private void pridejPredmety(Room prostor){
        seznamLehkychPredmetuVMistnosti.getChildren().clear();
        seznamTezkychPredmetuVMistnosti.getChildren().clear();
        vyberCil.getItems().clear();

        for(HObjekty predmet:prostor.getItems()){
            if(predmet.getWeight()<=2) {
                pridejPredmetDoMistnosti(predmet);
            }
            else{
                vyberCil.setVisible(true);
                pridejTezkePredmetyDoMistnosti(predmet);
                vyberCil.getItems().addAll(predmet.getName());
            }
        }
    }

    /**
     * Metoda zajišťující přidání sousedních prostorů
     * @param prostor
     */
    private void pridejVychody(Room prostor){
        seznamVychodu.getChildren().clear();
        for(Room p : prostor.getNeighbors()){
             HBox vychod = pridejSObrazkem(p.getName());

            seznamVychodu.getChildren().add(vychod);
            vychod.setOnMouseClicked(event -> {
                //vypisOdpoved(hra.executeCommand(pJDI + " " + p.getName()));
                odesliPrikaz(pJDI+" "+p.getName());
                if(jePruchodny(p)){
                zmenProstor(p);}
            });

        }

    }

    /**
     * Metoda zajišťující správné zobrazení předmětů v GUI a správné zpracování po kliknutí na předmět
     * @param predmet
     */
    private void pridejPredmetDoMistnosti(HObjekty predmet){
        HBox predmetSObrazkem = pridejSObrazkem(predmet.getName());
        seznamLehkychPredmetuVMistnosti.getChildren().add(predmetSObrazkem);

        predmetSObrazkem.setOnMouseClicked(event -> {
                if (predmet.getWeight()<=2) {
                    odesliPrikaz(pVEZMI+" "+predmet.getName());
                    HBox predmetVBatohu = pridejSObrazkem(predmet.getName());
                    vyberPredmet.getItems().addAll(predmet.getName());
                    seznamPredmetuVBatohu.getChildren().add(predmetVBatohu);
                    seznamLehkychPredmetuVMistnosti.getChildren().remove(predmetSObrazkem);

                    predmetVBatohu.setOnMouseClicked(event1 -> {
                        odesliPrikaz(pPOLOŽ+" "+predmet.getName());
                        seznamPredmetuVBatohu.getChildren().remove(predmetVBatohu);
                        vyberPredmet.getItems().remove(predmet.getName());
                        pridejPredmetDoMistnosti(predmet);
                    });
                }

        });
    }

    /**
     * Metoda přidává nezvednutelné předměty do určeného kontejneru
     * @param predmet
     */
    private void pridejTezkePredmetyDoMistnosti(HObjekty predmet){
        HBox predmetSObrazkem = pridejSObrazkem(predmet.getName());
        seznamTezkychPredmetuVMistnosti.getChildren().add(predmetSObrazkem);
        //SetOnMouse Click
    }

    /**
     * Metoda zajišťující vytvoření HBoxu, ve kterém se nachází název a obrázek zadaného objektu(prostoru, předmětu
     * @param string
     * @return HBox
     */
    private HBox pridejSObrazkem(String string){
        HBox objektSObrazkem = new HBox();
        objektSObrazkem.setSpacing(10);
        Label nazevObjektu = new Label(string);

        ImageView objektImageView = new ImageView();
        Image obrazekObjektu = new Image(getClass().getClassLoader().getResourceAsStream
                        (string.toLowerCase()+".jpg"));
        objektImageView.setFitHeight(VYSKA_IKONY);
        objektImageView.setFitWidth(SIRKA_IKONY);
        objektImageView.setImage(obrazekObjektu);
        objektSObrazkem.getChildren().addAll(objektImageView,nazevObjektu);
        return objektSObrazkem;

    }

    /**
     * Metoda reagující na stlačení tlačítka zpracováním zadaných hodnot v ComboBoxech
     */
    @FXML
    private void zpracujPrikazy(){
        String prikaz = vyberUkonu.getValue().toString();
        if(vyberPredmet.isVisible()){
            if(hra.getWorld().getCurrentPlace().toString()==LETOVÝ_MODUL&&!State.isPoweredUp()){
                prikaz += " "+vyberCil.getValue().toString();
            }
            else {prikaz += " " + vyberPredmet.getValue().toString();} }
        if(vyberCil.isVisible()){
            prikaz += " " + vyberCil.getValue().toString(); }


        odesliPrikaz(prikaz);

        vyberUkonu.getSelectionModel().clearSelection();
        vyberPredmet.getSelectionModel().clearSelection();
        vyberCil.getSelectionModel().clearSelection();
    }

    /**
     * Metoda zajišťující správné zobrazení ComboBoxu vzhledem k aktuálnímu prostoru
     * @param prostor
     */
    private void nastavComboBox(Room prostor){
        switch(prostor.getName()){
            case CHODBA: vyberPredmet.setVisible(false);
                lblPredmet.setVisible(false);
                break;
            case ZEMĚ: vyberCil.setVisible(false);
                lblCil.setVisible(false);
                break;
            default: vyberUkonu.setVisible(true);
            vyberPredmet.setVisible(true);
                vyberCil.setVisible(true);
                lblCil.setVisible(true);
                lblPredmet.setVisible(true);
                break;
        }
    }

    /**
     * Metoda nastavující text TextArea na odpověď programu na provedený příkaz
     * @param string
     */
    private void vypisOdpoved(String string){

        odpovediProgramu.setText(string);
    }

    /**
     * Metoda zjišťující průchodnost prostoru vzhledem ke stavu překážek
     * @param prostor
     * @return bool
     */
    private boolean jePruchodny(Room prostor){
        if(prostor.getName() == KONTROLNÍ_STANICE){
            if(State.isStationTerminalVerified()){
                return true;
            } else return false;
        }
        if(prostor.getName()== BIO_LABORATOŘ){
            if(State.isBaricadeBroken()){
                return true;
            } else return false;
        }
        if (prostor.getName()==VESMÍR){
            if(State.isModulTerminalVerified() && State.isPoweredUp()){
                return true;
            } else return false;
        }
        return true;
    }

    /**
     * Metoda odesílající příkaz logice programu, která zároveň kontroluje zda by neměl nastat konec
     * @param string
     */
    private void odesliPrikaz(String string){
        vypisOdpoved(hra.executeCommand(string));
        if(State.isPlanted()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Konec hry");
            alert.setHeaderText("Gratulace! Úspěšně jste dohráli hru!");
            alert.setContentText("Doufám, že se Vám hra líbila, pro opětovné spuštění hry"+
                    " zvolte v menu Nová hra");
            alert.showAndWait();

            vypisOdpoved(hra.executeCommand(zKONEC));
            hra.stop();
        }
    }

    /**
     * Reakce na kliknutí na tlačítko Nová hra, realizující novou hru
     * @param actionEvent
     */
    public void novaHra(ActionEvent actionEvent) {
        hra.stop();

        seznamPredmetuVBatohu.getChildren().clear();
        vyberUkonu.getItems().clear();
        vyberPredmet.getItems().clear();
        IGame game = new MirSpaceGame();
        game.executeCommand("");

        setHra(game);
    }

    /**
     * Reakce na kliknutí na tlačítko Konec, realizující ukončení spuštěného programu
     * @param actionEvent
     */
    public void konec(ActionEvent actionEvent) {
        System.exit(0);
    }

    /**
     * Reakce na kliknutí na tlačítko Nápověda, realizující zobrazení nápovědy
     * @param actionEvent
     */
    public void zobrazeniNapovedy(ActionEvent actionEvent) {
        WebView webView = new WebView();
        webView.getEngine().load(getClass().getResource("/napoveda.html").toExternalForm());
        Stage stage = new Stage();
        stage.setScene(new Scene(webView, 600, 850));
        stage.show();

    }
}
